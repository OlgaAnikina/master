insert into roles values(1,'ROLE_USER');
insert into roles values(2,'ROLE_ADMIN');
insert into roles values(3,'ROLE_MODERATOR');
insert into users(id,count_books,email,firstname,gender,lastname,password,username)
    values(1,0,'admin@mail.ru','Main',0,'Admin','123456','admin');
insert into users(id,count_books,email,firstname,gender,lastname,password,username)
    values(2,0,'user@mail.ru','Usual',0,'User','123456','user');
insert into users(id,count_books,email,firstname,gender,lastname,password,username)
    values(3,0,'moderator@mail.ru','Mod',0,'Moderator','123456','moderator');
insert into user_role values(1,3);
insert into user_role values(3,3);
