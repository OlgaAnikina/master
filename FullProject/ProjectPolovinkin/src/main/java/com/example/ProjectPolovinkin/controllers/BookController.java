package com.example.ProjectPolovinkin.controllers;

import com.example.ProjectPolovinkin.dto.TakeBookReq;
import com.example.ProjectPolovinkin.model.Book;
import com.example.ProjectPolovinkin.model.History;
import com.example.ProjectPolovinkin.model.User;
import com.example.ProjectPolovinkin.dto.history.PopularBook;
import com.example.ProjectPolovinkin.model.repositories.BookRepository;
import com.example.ProjectPolovinkin.model.repositories.HistoryRepository;
import com.example.ProjectPolovinkin.model.repositories.UserRepository;
import com.example.ProjectPolovinkin.dto.registarion.MessageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class BookController {
    @Autowired
    BookRepository bookRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    HistoryRepository historyRepository;

    @GetMapping("/books")
    public ResponseEntity<List<Book>> getAllBooks(@RequestParam(required = false) String name) {

        try {
            List<Book> books = new ArrayList<Book>();
            bookRepository.findAll().forEach(books::add);
            if (books.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(books, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/books/{id}")
    public ResponseEntity<Book> getBooksById(@PathVariable("id") long id) {
        Optional<Book> bookData = bookRepository.findById(id);
        if (bookData.isPresent()) {
            return new ResponseEntity<>(bookData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/books")
    public ResponseEntity<?> createBook(@RequestBody Book book) {
        if (bookRepository.existsByNameAndAuthor(book.getName(),book.getAuthor())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("This book is already exists"));
        }
        try {
            Book newBook = bookRepository
                    .save(new Book(book.getName(),book.getAuthor(),book.getCount(),book.getCount(),book.getDiscription(),book.getGenre(),book.getRating()));
            if (book.getRating() > 5) {
                return new ResponseEntity<>(HttpStatus.FAILED_DEPENDENCY);
            }
            return new ResponseEntity<>(newBook, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/books/{id}")
    public ResponseEntity<HttpStatus> deleteBook(@PathVariable("id") long id) {
        try {
            List<History> bookData = historyRepository.findAllByBookId(bookRepository.findById(id).get());
            for(History hist: bookData) {
                hist.setComebackDateTrue(Calendar.getInstance().getTime());
                hist.getUserId().setCountBooks(hist.getUserId().getCountBooks() - 1);
                userRepository.save(hist.getUserId());
                historyRepository.save(hist);
                historyRepository.deleteById(hist.getId());
            }
            bookRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/books/{id}")
    public ResponseEntity<?> updateBook(@PathVariable("id") long id, @RequestBody Book book) {
        Optional<Book> bookData = bookRepository.findById(id);
        if (bookData.isPresent()) {
            if (book.getRating() > 5) {
                return new ResponseEntity<>(HttpStatus.FAILED_DEPENDENCY);
            }
            if (book.getAvailableQuantity() > book.getCount()) {
                return ResponseEntity
                        .badRequest()
                        .body(new MessageResponse("Available quantity must be < count"));
            }
            Book newBook = bookData.get();
            newBook.setName(book.getName());
            newBook.setAuthor(book.getAuthor());
            newBook.setCount(book.getCount());
            newBook.setAvailableQuantity(book.getAvailableQuantity());
            newBook.setDiscription(book.getDiscription());
            newBook.setGenre(book.getGenre());
            newBook.setRating(book.getRating());
            return new ResponseEntity<>(bookRepository.save(newBook), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/books/take/{id}")
    public ResponseEntity<?> takeBook(@PathVariable("id") long id, @RequestBody TakeBookReq takeBookReq) {
        Optional<Book> bookData = bookRepository.findById(id);
        Optional<User> userData = userRepository.findById(takeBookReq.getUserInfo());
        Book newBook = bookData.get();
        User newUser = userData.get();
        if (newBook.getAvailableQuantity() > 0 && takeBookReq.getDays() > 0 && takeBookReq.getDays() < 100) {
            newBook.setAvailableQuantity(newBook.getAvailableQuantity() - 1);
            newUser.setCountBooks(newUser.getCountBooks() + 1);
            bookRepository.save(newBook);
            userRepository.save(newUser);
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, takeBookReq.getDays());
            History history = new History(newUser,newBook,cal.getTime(),Calendar.getInstance().getTime());
            historyRepository.save(history);
            return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/books/back/{id}")
    public ResponseEntity<?> backBook(@PathVariable("id") long id,@RequestBody Long userid) {

        Optional<Book> bookData = bookRepository.findById(id);
        Optional<User> userData = userRepository.findById(userid);
        Book newBook = bookData.get();
        User newUser = userData.get();
        List <History> historyList = new ArrayList<>();
        historyRepository.findByBookIdAndUserIdOrderByIdDesc(newBook,newUser).forEach(historyList::add);
        historyList.get(0).setComebackDateTrue(Calendar.getInstance().getTime());
        newBook.setAvailableQuantity(newBook.getAvailableQuantity() + 1);
        newUser.setCountBooks(newUser.getCountBooks() - 1);
        historyRepository.save(historyList.get(0));
        bookRepository.save(newBook);
        userRepository.save(newUser);
        return ResponseEntity.ok(new MessageResponse("Successfully!"));
    }
    @GetMapping("/books/popular")
    public ResponseEntity<List<PopularBook>> getPopularBooks(@RequestParam(required = false) String name) {
        try {
            List<List<Long>> list = new ArrayList<>();
            historyRepository.findPopularBooks().forEach(list::add);
            List<PopularBook> popularBooks = new ArrayList<PopularBook>();
            for (List<Long> lst: list) {
                Optional<Book> bookData = bookRepository.findById(lst.get(0));
                PopularBook popularBook = new PopularBook(lst.get(0),bookData.get().getName(),bookData.get().getAuthor(),(int)(long)lst.get(1));
                popularBooks.add(popularBook);
            }

            if (popularBooks.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(popularBooks, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/userBooks/{id}")
    public ResponseEntity<List<Book>> getUserBooks(@PathVariable("id") long id) {
        Optional<User> userDate = userRepository.findById(id);
        List<History> historyDate = new ArrayList<>();
        historyDate = historyRepository.findAllByUserIdAndComebackDateTrueIsNull(userDate.get());
        List<Book> bookData = new ArrayList<>();
        for (History hist: historyDate) {
            bookData.add(hist.getBookId());
        }
            return new ResponseEntity<>(bookData, HttpStatus.OK);
    }

    @GetMapping("/userBooksReturn/{id}")
    public ResponseEntity<List<Book>> getUserBooksReturn(@PathVariable("id") long id) {
        List<Long> bookId = new ArrayList<>();
        historyRepository.getCreditBooksUser(id).forEach(bookId::add);
        List<Book> bookData = new ArrayList<>();
        for (Long i: bookId) {
            Optional<Book> book = bookRepository.findById(i);
            bookData.add(book.get());
        }
        return new ResponseEntity<>(bookData, HttpStatus.OK);
    }


}
