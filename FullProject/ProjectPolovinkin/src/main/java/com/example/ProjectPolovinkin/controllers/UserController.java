package com.example.ProjectPolovinkin.controllers;

import com.example.ProjectPolovinkin.dto.history.PopularBook;
import com.example.ProjectPolovinkin.dto.registarion.MessageResponse;
import com.example.ProjectPolovinkin.model.*;

import com.example.ProjectPolovinkin.dto.history.PopularUsers;
import com.example.ProjectPolovinkin.dto.history.UsersCredits;
import com.example.ProjectPolovinkin.model.repositories.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    HistoryRepository historyRepository;

    @Autowired
    BookRepository bookRepository;

    @Autowired
    RoleRepository roleRepository;



    @GetMapping("/users")
    public ResponseEntity<List<User>> getAllBooks(@RequestParam(required = false) String name) {
        try {
            List<User> users = new ArrayList<User>();
            userRepository.findAll().forEach(users::add);
            if (users.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(users, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/users/{id}")
    public ResponseEntity<User> getUserById(@PathVariable("id") long id) {
        Optional<User> userData = userRepository.findById(id);
        if (userData.isPresent()) {
            return new ResponseEntity<>(userData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @GetMapping("/users/popular")
    public ResponseEntity<List<PopularUsers>> getAllPopular(@RequestParam(required = false) String name) {
        try {
            List<List<Long>> list = new ArrayList<>();
            historyRepository.findPopularUsers().forEach(list::add);
            List<PopularUsers> popularUsers = new ArrayList<PopularUsers>();
            for (List<Long> lst: list) {
                Optional<User> userData = userRepository.findById(lst.get(0));
                PopularUsers popularUser = new PopularUsers(userData.get().getUsername(),userData.get().getFirstName(),userData.get().getLastName(),(int)(long)lst.get(1),userData.get().getId());
                popularUsers.add(popularUser);
            }
            if (popularUsers.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(popularUsers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/users/bad")
    public ResponseEntity<List<UsersCredits>> getAllBad(@RequestParam(required = false) String name) {
        try {
            List<List<Long>> list = new ArrayList<>();
            historyRepository.findUserCredit().forEach(list::add);
            List<UsersCredits> usersCredits = new ArrayList<UsersCredits>();
            for (List<Long> lst: list) {
                List<Long> books= new ArrayList<>();
                historyRepository.getCreditBooksUser(lst.get(0)).forEach(books::add);

                Set<Book> bookCredit = new HashSet<>();
                for (Long book: books) {
                    Optional<Book> b = bookRepository.findById(book);
                    bookCredit.add(b.get());
                }
                Optional<User> userData = userRepository.findById(lst.get(0));
                UsersCredits usersCredit = new UsersCredits(lst.get(0),userData.get().getUsername(),(int)(long)lst.get(1),bookCredit);
                usersCredits.add(usersCredit);
            }
            if (usersCredits.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(usersCredits, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<?> addRole(@PathVariable("id") long id, @RequestBody User user) {
        Optional<User> userDate = userRepository.findById(id);
        if (userDate.isPresent()) {
            User newUser = userDate.get();
            Set<Role> roles = newUser.getRoles();
            Role userRole = roleRepository
                    .findByName(ERole.ROLE_MODERATOR)
                    .orElseThrow(() -> new RuntimeException("Error, Role Mod is not found"));
            roles.add(userRole);
            newUser.setRoles(roles);
            userRepository.save(newUser);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/users/delRole/{id}")
    public ResponseEntity<?> delRole(@PathVariable("id") long id, @RequestBody User user) {
        Optional<User> userDate = userRepository.findById(id);
        if (userDate.isPresent()) {
            User newUser = userDate.get();
            Set<Role> roles = newUser.getRoles();
            Role userRole = roleRepository
                    .findByName(ERole.ROLE_MODERATOR)
                    .orElseThrow(() -> new RuntimeException("Error, Role Mod is not found"));
            roles.remove(userRole);
            newUser.setRoles(roles);
            userRepository.save(newUser);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/userUpdate/{id}")
    public ResponseEntity<?> updateTutorial(@PathVariable("id") long id, @RequestBody User user) {
        Optional<User> userData = userRepository.findById(id);
        if (userRepository.existsByEmail(user.getEmail()) && !user.getEmail().equals(userData.get().getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }
        if (userData.isPresent()) {
            User newUser = userData.get();
            newUser.setFirstName(user.getFirstName());
            newUser.setLastName(user.getLastName());
            newUser.setEmail(user.getEmail());
            newUser.setGender(user.getGender());
            return new ResponseEntity<>(userRepository.save(newUser), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
