package com.example.ProjectPolovinkin.controllers;


import com.example.ProjectPolovinkin.dto.history.UsedBook;
import com.example.ProjectPolovinkin.model.History;
import com.example.ProjectPolovinkin.model.repositories.BookRepository;
import com.example.ProjectPolovinkin.model.repositories.HistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class HistoryController {

    @Autowired
    HistoryRepository historyRepository;

    @Autowired
    BookRepository bookRepository;

    @GetMapping("/history")
    public ResponseEntity<List<History>> getAllHistory(@RequestParam(required = false) String name) {
        try {
            List<History> histories = new ArrayList<History>();
            historyRepository.findAll().forEach(histories::add);
            if (histories.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(histories, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/history/usedBook")
    public ResponseEntity<List<UsedBook>> getUsedBook (@RequestParam(required = false) String name) {
        List<History> userBook = new ArrayList<>();
        historyRepository.findByComebackDateTrueIsNull().forEach(userBook::add);

        List<UsedBook> usedBooks= new ArrayList<>();
        for (History book: userBook) {
            Boolean credit = book.getDateComeback().before(Calendar.getInstance().getTime());
            UsedBook usedBook = new UsedBook(book.getUserId(),book.getBookId(),credit);
            usedBooks.add(usedBook);
        }
        return new ResponseEntity<>(usedBooks, HttpStatus.OK);
    }
}
