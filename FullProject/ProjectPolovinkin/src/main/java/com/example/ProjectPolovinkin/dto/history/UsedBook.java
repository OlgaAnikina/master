package com.example.ProjectPolovinkin.dto.history;

import com.example.ProjectPolovinkin.model.Book;
import com.example.ProjectPolovinkin.model.User;

public class UsedBook {
    private User userId;
    private Book bookId;
    private Boolean credit;


    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Book getBookId() {
        return bookId;
    }

    public void setBookId(Book bookId) {
        this.bookId = bookId;
    }

    public Boolean getCredit() {
        return credit;
    }

    public void setCredit(Boolean credit) {
        this.credit = credit;
    }

    public UsedBook(User userId, Book bookId, Boolean credit) {
        this.userId = userId;
        this.bookId = bookId;
        this.credit = credit;
    }
}
