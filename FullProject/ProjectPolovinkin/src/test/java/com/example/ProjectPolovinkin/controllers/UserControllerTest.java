package com.example.ProjectPolovinkin.controllers;

import com.example.ProjectPolovinkin.model.History;
import com.example.ProjectPolovinkin.model.User;
import com.example.ProjectPolovinkin.model.repositories.BookRepository;
import com.example.ProjectPolovinkin.model.repositories.HistoryRepository;
import com.example.ProjectPolovinkin.model.repositories.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserControllerTest {

    @Autowired
    UserRepository userRepository;
    @Autowired
    BookRepository bookRepository;
    @Autowired
    HistoryRepository historyRepository;

    @Test
    void notNullUsers() {
        List<User> listUser = new ArrayList<>();
        userRepository.findAll().forEach(listUser::add);
        userRepository.findAll().forEach(Assertions::assertNotNull);
    }

    @Test
    void UsersHaveMoreOneRoles() {
        List<User> listUser = new ArrayList<>();
        userRepository.findAll().forEach(listUser::add);
        listUser.forEach((User user) -> {
            assertNotNull(user.getRoles());
        });
    }

    @Test
    void countTakenEqualsCountBooks() {
        List<User> listUser = new ArrayList<>();
        userRepository.findAll().forEach(listUser::add);
        listUser.forEach((User user) -> {
            System.out.println(user);
            List<History> history = new ArrayList<>();
            historyRepository.findAllByUserIdAndComebackDateTrueIsNull(user).forEach(history::add);
            assertEquals(history.size(), (int) user.getCountBooks());
        });
    }
}
