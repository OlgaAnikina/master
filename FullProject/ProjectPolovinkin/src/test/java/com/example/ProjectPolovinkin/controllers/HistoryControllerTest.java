package com.example.ProjectPolovinkin.controllers;

import com.example.ProjectPolovinkin.model.History;
import com.example.ProjectPolovinkin.model.repositories.HistoryRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class HistoryControllerTest {

    @Autowired
    HistoryRepository historyRepository;

    @Test
    void dateTakeBeforeDateComeback() {
        List<History> histories = new ArrayList<>();
        historyRepository.findAll().forEach(histories::add);
        for(History hist: histories) {
            assertTrue(hist.getDateTake().before(hist.getDateComeback()));
        }
    }
    @Test
    void haveNull() {
        List<History> histories = new ArrayList<>();
        historyRepository.findAll().forEach(histories::add);
        for(History hist: histories) {
            assertNotNull(hist);
        }
    }
}
