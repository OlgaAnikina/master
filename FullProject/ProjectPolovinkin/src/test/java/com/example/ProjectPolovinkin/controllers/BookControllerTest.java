package com.example.ProjectPolovinkin.controllers;

import com.example.ProjectPolovinkin.model.Book;
import com.example.ProjectPolovinkin.model.Comment;
import com.example.ProjectPolovinkin.model.Genre;
import com.example.ProjectPolovinkin.model.repositories.BookRepository;
import com.example.ProjectPolovinkin.model.repositories.CommentRepository;
import com.example.ProjectPolovinkin.model.repositories.RoleRepository;
import com.example.ProjectPolovinkin.model.repositories.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.Rollback;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class BookControllerTest {
    @Autowired
    BookRepository bookRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    CommentRepository commentRepository;
    @Autowired
    RoleRepository roleRepository;

    @Test
    void getBooksById() {
        Optional<Book> trueBook = bookRepository.findById(22L);
        List<Comment> comments = new ArrayList<>();
        Book newBook = new Book(22,"Bible","God",3,1,comments,"This book about man which make live on Earth",Genre.FANTASY,0);
        Assertions.assertEquals(newBook.getId(),trueBook.get().getId());
        Assertions.assertEquals(newBook.getName(),trueBook.get().getName());
        Assertions.assertEquals(newBook.getAuthor(),trueBook.get().getAuthor());
        Assertions.assertEquals(newBook.getCount(),trueBook.get().getCount());
        Assertions.assertEquals(newBook.getRating(),trueBook.get().getRating());
        Assertions.assertEquals(newBook.getAvailableQuantity(),trueBook.get().getAvailableQuantity());
    }
    @Test
    void notNullBooks() {
        List<Book> listBook = new ArrayList<>();
        bookRepository.findAll().forEach(listBook::add);
        bookRepository.findAll().forEach(Assertions::assertNotNull);
    }

    @Test
    void availableQuantityLessThenCount() {
        List<Book> listBook = new ArrayList<>();
        bookRepository.findAll().forEach(listBook::add);
        listBook.forEach((Book book) -> {
            assertTrue(book.getCount() >= book.getAvailableQuantity());
        });
    }

}
