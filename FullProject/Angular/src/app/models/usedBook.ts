import {User} from './user';
import {Book} from './book';

export class UsedBook {
  userId: User;
  bookId: Book;
  credit: boolean;
}
