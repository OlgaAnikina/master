import { Book} from '../models/book';
import { Component, OnInit, Input } from '@angular/core';
import { BookService} from '../services/book.service';
import { Router, ActivatedRoute } from '@angular/router';
import {Observable} from 'rxjs';
import {TokenStorageService} from '../auth/token-storage.service';
import {User} from '../models/user';
import {UserService} from '../services/user.service';
import {TakeInfo} from '../models/takeInfo';
import {Comment} from '../models/comment';
import {CommentService} from '../services/comment.service';
import {Validators} from '@angular/forms';
import {FormBuilder} from '@angular/forms';
import {UserBookService} from '../services/user-book.service';


@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {

  private roles: string[] = [];
  showAdminBoard = false;
  id: number | undefined;
  book: Book | undefined;
  books: Observable<Book[]> | undefined;
  user: User;
  idUser: number | undefined;
  users: Observable<User[]> | undefined;
  takeInfo: TakeInfo;
  newDays: number;
  comment: Comment = new Comment();
  submitted = false;
  comments: Observable<Comment[]>;
  // tslint:disable-next-line:variable-name
  new_comment: any;
  error: any;
  form: any;
  p = 1;
  showModeratorBoard = false;
  isDelete = false;
  delCom = false;
  idCom: number;
  booksForReturn: Observable<Book[]>| undefined;
  cb: boolean;

  constructor(private route: ActivatedRoute, private router: Router,
              private bookService: BookService, private token: TokenStorageService,
              private userService: UserService, private commentService: CommentService,
              private fb: FormBuilder, private userBookService: UserBookService) {
  }

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.cb = true;
    const user = this.token.getUser();
    this.roles = user.roles;
    this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
    this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
    this.takeInfo = new TakeInfo();
    this.takeInfo.days = 30;
    this.takeInfo.userInfo = this.token.getUser().id;
    // tslint:disable-next-line:variable-name
    let new_user: User;
    this.book = new Book();
    // this.taken = this.checkBook(this.id);
    this.booksForReturn = this.userBookService.getUserBookList(this.token.getUser().id);
    this.booksForReturn.forEach(value => {
      console.log(value);
      value.forEach(value2 => {
        if (value2.id == this.id) {
          this.cb = false;
        }
      });
    });
    this.bookService.getBook(this.id)
      .subscribe(data => {
        this.book = data;
        this.comment.book = this.book;
      }, error => console.log(error));
    // @ts-ignore
    this.comments = this.commentService.getComments(this.id).subscribe(
      data => {
        this.new_comment = data;
        this.comments = data;
      }
    );
    this.user = new User();
    this.idUser = this.token.getUser().id;
    this.userService.getUser(this.idUser)
      .subscribe(data => {
        this.user = data;
        this.comment.author = this.user;
        // tslint:disable-next-line:variable-name
        new_user = data;
      }, error => {
        console.log(error);
      });
    this.form = this.fb.group({
      days: ['', {
        validators: [
          Validators.required,
          Validators.max(100),
          Validators.min(1)
        ],
        updateOn: 'blur'
      }],
      com: ['', {
        validators: [
          Validators.required,
          Validators.maxLength(10)
        ],
        updateOn: 'blur'
      }],
    });

  }

  // tslint:disable-next-line:typedef
  takeBook(getDays: number, userInfo: number) {
    this.takeInfo.days = getDays;
    this.takeInfo.userInfo = userInfo;

    this.bookService.takeBook(this.id, this.takeInfo)
      .subscribe(data => {
        this.book = new Book();
        this.takeInfo = new TakeInfo();
        window.location.reload();
      },  error => {
        this.error = error.status;
      });
  }

  // tslint:disable-next-line:typedef
  backBook() {
    this.bookService.backBook(this.id, this.token.getUser().id)
      .subscribe(data => {
        this.book = new Book();
        window.location.reload();
      }, error => console.log(error));
  }

  // tslint:disable-next-line:typedef
  list() {
    this.router.navigate(['profile']);
  }

  // tslint:disable-next-line:typedef
  addComment() {
    this.router.navigate(['create-comment']);
  }

  // tslint:disable-next-line:typedef
  checkBook() {
    this.booksForReturn = this.userBookService.getUserBookList(this.token.getUser().id);
  }
  newEmployee(): void {
    this.submitted = false;
    this.comment = new Comment();
  }

  // tslint:disable-next-line:typedef
  save() {
    this.commentService
      .createComment(this.comment).subscribe(data => {
        this.comment = new Comment();
      },
      error => console.log(error));
  }

  // tslint:disable-next-line:typedef
  onSubmit() {
    this.submitted = true;
    this.save();
  }

  // tslint:disable-next-line:typedef
  reloadPage(): void {
    window.location.reload();
  }

  // tslint:disable-next-line:typedef
  updateComment(id: number) {
    this.router.navigate(['update-comment', id]);
  }

  userDetails(id: number){
    this.router.navigate(['user-details', id]);
  }

  deleteComment(id: number) {
    this.commentService.deleteComment(id)
      .subscribe(
        data => {
          this.reloadPage();
        },
        error => console.log(error));
  }

  // tslint:disable-next-line:typedef
  deleteBook(id: number) {
    this.bookService.deleteBook(id)
      .subscribe(
        data => {
          console.log(data);
          this.router.navigate(['book-list']);
        },
        error => console.log(error));
  }
  // tslint:disable-next-line:typedef
  updateBook(id: number) {
    this.router.navigate(['update', id]);
  }
}
