import { Component, OnInit } from '@angular/core';
import {Book} from '../models/book';
import {ActivatedRoute, Router} from '@angular/router';
import {BookService} from '../services/book.service';
import {Location} from '@angular/common';
import {User} from '../models/user';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {

  id: number;
  user: User;
  submitted = false;
  errorMessage = '';
  isSignUpFailed = false;
  isSuccessful = false;

  constructor(private route: ActivatedRoute, private router: Router,
              private userService: UserService, private location: Location) { }

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.user = new User();
    this.id = this.route.snapshot.params.id;
    this.userService.getUser(this.id)
      .subscribe(data => {
          console.log(data);
          this.user = data;
        },       err => {
          this.errorMessage = err.error.message;
        }
      );
  }

  // tslint:disable-next-line:typedef
  updateUser() {
    this.userService.updateUser(this.id, this.user)
      .subscribe(data => {
          console.log(data);
          this.user = new User();
          this.gotoList();
          this.isSignUpFailed = false;
          this.isSuccessful = true;
          console.log(this.isSuccessful);
        },       err => {
          this.errorMessage = err.error.message;
          this.isSignUpFailed = true;
          console.log(err.error.message);
          console.log(this.isSuccessful);
        }
      );
  }

  // tslint:disable-next-line:typedef
  onSubmit() {
    this.updateUser();
    this.submitted = true;
  }

  // tslint:disable-next-line:typedef
  gotoList() {
    this.router.navigate(['/profile']);
  }

}
