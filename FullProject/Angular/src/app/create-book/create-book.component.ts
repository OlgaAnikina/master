import { BookService} from '../services/book.service';
import { Book} from '../models/book';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.component.html',
  styleUrls: ['./create-book.component.css']
})
export class CreateBookComponent implements OnInit {

  book: Book = new Book();
  submitted = false;
  isSignUpFailed = false;
  errorMessage = '';
  isSuccessful = false;

  constructor(private bookService: BookService,
              private router: Router) { }

  // tslint:disable-next-line:typedef
  ngOnInit() {
  }
  newEmployee(): void {
    this.submitted = false;
    this.book = new Book();
  }

  // tslint:disable-next-line:typedef
  save() {
    this.bookService
      .createBook(this.book).subscribe(data => {
        console.log(data);
        this.book = new Book();
        this.gotoList();
        this.isSuccessful = true;
        this.isSignUpFailed = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }

  // tslint:disable-next-line:typedef
  onSubmit() {
    this.submitted = true;
    this.save();

  }

  // tslint:disable-next-line:typedef
  gotoList() {
    this.router.navigate(['/api/books']);
  }
  // tslint:disable-next-line:typedef
  checkNull(some: any): boolean{
    if (some == null) {
      return true;
    } else {
      return false;
    }
  }


}
