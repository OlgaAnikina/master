import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserBookService {

  constructor(private http: HttpClient) { }

  getUserBookReturned(id: number | undefined): Observable<any> {
    return this.http.get(`http://localhost:8080/api/userBooksReturn/${id}`);
  }
  getUserBookList(id: number | undefined): Observable<any> {
    return this.http.get(`http://localhost:8080/api/userBooks/${id}`);
  }
}
